import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Dots extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField playerOne;
	private JTextField playerTwo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Dots frame = new Dots();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dots() {
		setTitle("Dots");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 431, 541);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		playerOne = new JTextField();
		playerOne.setText("Player 1");
		playerOne.setBounds(10, 10, 115, 25);
		contentPane.add(playerOne);
		playerOne.setColumns(10);
		
		playerTwo = new JTextField();
		playerTwo.setText("Player 2");
		playerTwo.setBounds(135, 10, 110, 25);
		contentPane.add(playerTwo);
		playerTwo.setColumns(10);
		
		JButton resetBtn = new JButton("Reset");
		resetBtn.setBounds(305, 10, 100, 25);
		contentPane.add(resetBtn);
	
		int w=8;
		int l=8;
		Box[][] board = new Box[w][l];
		for (int i=0; i<w; i++) {
			for (int j=0; j<l; j++) {
				board[i][j] = new Box();
			}
		}
		
		
		GamePanel gamePanel = new GamePanel(board);
		gamePanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { //occurs when the user clicks on the game board
				int X = (e.getX() - 15);
				int Y = (e.getY() -15);
				
				int distX = X % 50;
				int distY = Y % 50;
				
				int invDistX = 50 - distX;
				int invDistY = 50 - distY;
				
				int colIndex = Y / 50; //holds the index of the column that was clicked 
				int rowIndex = X / 50; //holds the index of the row that was clicked
				//box [0][0] will be the top left box
				
				if((distX < distY) && (distX < invDistX) && (distX < invDistY)) { //the user clicked the left side of the box
					//draw a line on the left side of the box 
					board[rowIndex][colIndex].setLeft(true);
					//System.out.println("Box[" + rowIndex + "][" + colIndex + "] left side");

					if(rowIndex != 0) { //if this box is not on the left side of the game board
						board[rowIndex - 1][colIndex].setRight(true);
					}
				}
				else if((distY < distX) && (distY < invDistX) && (distY < invDistY)) { //the user clicked the top of the box
					//draw a line on the top of the box 
					board[rowIndex][colIndex].setTop(true);
					//System.out.println("Box[" + rowIndex + "][" + colIndex + "] top side");

					if(colIndex != 0) { //if this box is not on the top of the game board
						board[rowIndex][colIndex - 1].setBot(true);
					}
				}
				else if((invDistX < distX) && (invDistX < distY) && (invDistX < invDistY)) { //the user clicked the right side of the box
					//draw a line on the right side of the box
					board[rowIndex][colIndex].setRight(true);
					//System.out.println("Box[" + rowIndex + "][" + colIndex + "] right side");

					if(rowIndex < w) { //if this box is not on the right side of the game board
						board[rowIndex + 1][colIndex].setLeft(true);
					}
				}
				else if((invDistY < distX) && (invDistY < distY) && (invDistY < invDistX)) { //the user clicked the bottom of the box
					//draw a line on the bottom of the box
					board[rowIndex][colIndex].setBot(true);
					//System.out.println("Box[" + rowIndex + "][" + colIndex + "] bottom side");

					if(colIndex < l) { //if this box is not on the bottom of the game board
						board[rowIndex][colIndex + 1].setTop(true);
					}
				}
				else { //all distances are equal
					//do nothing
				}
				
				gamePanel.repaint(); //request for the game board to be updated
				
			}
		});
		gamePanel.setBounds(10, 75, 395, 395);
		contentPane.add(gamePanel);
		
		JLabel playerOneScore = new JLabel("Player 1's Score:");
		playerOneScore.setBounds(10, 40, 115, 20);
		contentPane.add(playerOneScore);
		
		JLabel playerTwoScore = new JLabel("Player 2's Score:");
		playerTwoScore.setBounds(135, 40, 100, 20);
		contentPane.add(playerTwoScore);
	}
}
