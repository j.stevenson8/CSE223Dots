import javax.swing.JButton;

public class MyButton extends JButton {
	GamePanel gamePanel;
	
	public MyButton(GamePanel panel) {
		gamePanel = panel;
		return;
	}
	
	public void setPanel(GamePanel panel) {
		gamePanel = panel;
		return;
	}
}
