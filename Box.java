
public class Box {
	private boolean top, bot, left, right = false;
	char player;
	
	public Box()
	{
	}
	
	public void setTop(boolean a)
	{
		top = a;
		return;
	}
	
	public void setBot(boolean a)
	{
		bot = a;
		return;
	}
	
	public void setLeft(boolean a)
	{
		left = a;
		return;
	}
	
	public void setRight(boolean a)
	{
		right = a;
		return;
	}
	
	public boolean getTop()
	{
		return top;
	}
	
	public boolean getBot()
	{
		return bot;
	}
	
	public boolean getLeft()
	{
		return left;
	}
	
	public boolean getRight()
	{
		return right;
	}
	
	public void setPlayer(String name)
	{
		player = name.charAt(0);
		return;
	}
	
	public String getPlayer()
	{
		return String.valueOf(player);
	}
	
	public boolean isComplete() {
		if(right && left && top && bot) {
			return true;
		}
		return false;
	}
	
	public void init() {
		setRight(false);
		setTop(false);
		setLeft(false);
		setBot(false);
		return;
	}
}
