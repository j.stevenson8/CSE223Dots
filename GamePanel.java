import java.awt.Graphics;
import javax.swing.JPanel;

public class GamePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width;
	private int length;
	private Box[][] board;
	String playerOne = "";
	String playerTwo = "";
	boolean turn = true;
	
	public GamePanel(int w, int l) {
		width = w;
		length = l;
		return;
	}
	
	public GamePanel(Box[][] gameBoard) {
		board = gameBoard;
		width = 8;
		length = 8;
		return;
	}
	
	public void paint(Graphics g) {
		//draws new stuff onto game board
		super.paint(g);
		int X, Y;
		int size = 7;
		
		for (int i=0; i<=width; i++) {
			X=i*50 + 15;
			for (int j=0; j<=length; j++) {
				Y=j*50 + 15;
				g.fillOval(X,Y,size,size);
			}
		}
	
		for (int i=0; i<width; i++) {
			for (int j=0; j<length; j++) {
				if(board[i][j].getTop()) { //draw a line at the top of the box
					g.drawLine(19 + i * 50, 19 + j * 50, 69 + i * 50, 19 + j * 50);
				}
				if(board[i][j].getBot()) { //draw a line at the bottom of the box
					g.drawLine(19 + i * 50, 69 + j * 50, 69 + i * 50, 69 + j * 50);
				}
				if(board[i][j].getLeft()) { //draw a line on the left side of the box
					g.drawLine(19 + i * 50, 19 + j * 50, 19 + i * 50, 69 + j * 50);
				}
				if(board[i][j].getRight()) { //draw a line on the right side of the box
					g.drawLine(69 + i * 50, 19 + j * 50, 69 + i * 50, 69 + j * 50);
				}
				
				if(board[i][j].isComplete()) {
					g.drawString(board[i][j].getPlayer(), 40 + i * 50, 45 + j * 50);					
				}
			}
		}
	}
	
	public void setPlayerOne(String p) {
		playerOne = p;
		return;
	}

	public void setPlayerTwo(String p) {
		playerTwo = p;
		return;
	}

	public String getPlayerOne() {
		return playerOne;
	}

	public String getPlayerTwo() {
		return playerTwo;
	}
	
	public void init() {
		for (int i=0; i<width; i++) {
			for (int j=0; j<length; j++) {
				board[i][j].init();
			}
		}
		repaint();
	}
}
